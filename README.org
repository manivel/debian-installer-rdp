#+TITLE: Remastering a Debian DVD ISO for use with the RDP Thinbook
#+AUTHOR: Kumar Appaiah
#+OPTIONS: toc:nil date:nil

* Introduction

This document describes the steps to remaster a Debian ISO to create
an installer for the RDP Thinbook. This has been tested with a
~stable~ (~stretch~) ISO. These steps can redone for you to create
your own custom ISO.

Note that you can rebuilding the kernel is optional, but is probably
required to use all the features of the laptop.

If you are on a recent Debian machine (or Ubuntu machine) with
internet working, first install the prerequisites listed in the
section below. Then, just running the following should build a working
ISO (currently ~stretch~ or ~buster~):

#+BEGIN_SRC text :exports code
sudo sh make-custom-iso.sh <isoimage>
#+END_SRC

* Prerequisites
I am assuming that you are using a fairly recent Debian or
Debian-based system which has at least 10 GB of free space. This has
been done on a Debian stretch system, but should work on any recent
Debian or Ubuntu machine.

Get the prerequisites using:

#+BEGIN_SRC bash :exports code
apt install -y bsdtar devscripts xorriso apt-utils cpio isolinux
#+END_SRC

* Grabbing and extracting an ISO
For the purposes of this test, I preferred to use an ISO of the first
DVD (generally ~debian-<version>-amd64-DVD-1.iso~) available [[https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/][here]]. Put
it in a directory and extract it as follows:
#+BEGIN_SRC text :exports code
rm -rf cd
mkdir cd
bsdtar -C cd -xf debian-9.3.0-amd64-DVD-1.iso
#+END_SRC

* Rebuilding/replacing the kernel
In this approach, we compile a kernel using the upsream source, and
then ask the installer to install it at the end. To do so, get a
fairly recent Debian kernel (or upstream kernel). One approach is the following:

  1. Get a recent kernel source. On Debian, you can do something like this:
     #+BEGIN_SRC bash :exports code
     apt build-dep linux && apt install linux-source-4.14
     #+END_SRC
  2. In your home directory, extract it and build it:
     #+BEGIN_SRC bash :exports code
     tar Jxfv /usr/src/linux-source-4.14.tar.xz
     #+END_SRC
  3. Patch the kernel: get the patch files from [[https://github.com/sundarnagarajan/kernel_build/tree/master/patches][Sundar Nagarajan's
     repository]] and apply them. Assuming they are in the parent directory:
     #+BEGIN_SRC bash :exports code
     patch -p1 < ../001_rfkill_bluetooth_rtl8723bs_bt.patch
     #+END_SRC
  4. Get a decent configuration file, again, like [[https://github.com/sundarnagarajan/kernel_build/blob/master/config.kernel][Sundar Nagarajan's]],
     and store it as ~.config~. Update the configuration:
     #+BEGIN_SRC bash :exports code
     make olddefconfig
     #+END_SRC
  5. *TODO*: update the changelog and version: have to add this.
  6. Build the kernel:
     #+BEGIN_SRC bash :exports code
     MAKEFLAGS="CONCURRENCY_LEVEL=5 CCACHE_PREFIX=distcc" make-kpkg --rootcmd fakeroot --initrd --append-to-version=-0-iitb-amd64 kernel-image kernel-headers kernel-source
     #+END_SRC
     This takes a good while, and generates the kernel and
     corresponding packages.
  7. Add the deb files generated to the CD pool. In particular, copy the following files (your version numbers may
     be different):
     #+BEGIN_SRC text :exports code
     linux-headers-4.14.13-0-iitb-amd64_4.14.13-iitb-amd64-10.00.Custom_amd64.deb
     linux-image-4.14.13-0-iitb-amd64_4.14.13-iitb-amd64-10.00.Custom_amd64.deb
     #+END_SRC
     into your CD path at ~cd/pool/main/l/linux~. If you are using the
     ~make-custom-iso.sh~ script, put these deb package within the
     ~kernel-packages~ subdirectory here before running it, and the
     script will put it in the right place for you.

You have now successfully added the kernel for the new
image. Alternately, you can just replace the kernel in
~cd/pool/main/l/linux/~ with a more recent pre-built one. In addition,
we add the kernel to the preseed configuration (see below).

* Adding additional firmware and packages
You should also add additional firmware at this stage. You should get
the Realtek non-free firmware as well as the Intel sound firmware and
place it in ~./cd/pool/non-free/f/firmware-nonfree/~ (create that
directory). Check the lines in the [[file:make-custom-iso.sh][make-custom-iso.sh]] for details.

In addition, the rfkill Debian package should also be included. You
can download the deb package and place it in
~./cd/pool/main/r/rfkill/~. See [[file:make-custom-iso.sh][make-custom-iso.sh]] for an example.

* Adding patched rfkill
The rfkill module needs a one-line patch. This is provided in a deb
package in this repository. The source code is available [[https://github.com/kumanna/rfkill-dkms][here]].
* Extra packages
Any packages you place within the ~extra-packages~ directory will be
added to the image. Note that no dependency checks are done, and the
packages will not be installed. You can use the preseed settings
(discussed below) to get them installed automatically.
* Create new Release and Packages files
To ensure that the altered packages are configured correctly, first
download the overrides file for the distribution, such as
[[http://ftp.de.debian.org/debian/indices/override.buster.main.gz][http://ftp.de.debian.org/debian/indices/override.stretch.main.gz]]. Extract
it in a folder called ~indices~ into a file called ~override~ in the
parent directory of the ~cd~ directory. If you have added packages,
such as ~firmware-realtek~ and ~firmware-intel-sound~, add an entry
for these in the file like this:

#+BEGIN_SRC text :exports code
firmware-realtek	optional	non-free/kernel
firmware-intel-sound	optional	non-free/kernel
#+END_SRC

Create the directory and Package file:
#+BEGIN_SRC shell :exports code
cd cd
mkdir -p dists/stretch/non-free/binary-amd64
dpkg-scanpackages pool/non-free/ > dists/stretch/non-free/binary-amd64/Packages
dpkg-scanpackages pool/main/ > dists/stretch/main/binary-amd64/Packages
cd ..
#+END_SRC

#+BEGIN_COMMENT
Then adapt the ~[[file:config-udeb][config-udeb]]~, ~[[file:config-deb][config-udeb]]~ and ~[[file:config-rel][config-rel]]~ files in
this repository (they are present with a ~.orig~ suffix) to
the parent directory of ~cd~ and run:

#+BEGIN_SRC shell :exports code
apt-ftparchive generate config-udeb
apt-ftparchive generate config-deb
apt-ftparchive -c config-rel release cd/dists/stretch > cd/dists/stretch/Release
#+END_SRC

* Fix md5sums
This should do:
#+BEGIN_SRC shell :exports code
cd cd; md5sum `find ! -name "md5sum.txt" ! -path "./isolinux/*" -follow -type f` > md5sum.txt; cd ..
#+END_SRC

* Preseeding the installer
First edit the kernel version in the preseed file
~example-preseed.txt~ to specify the correct kernel version, like
this:
#+BEGIN_SRC shell :exports code
d-i base-installer/kernel/image string linux-image-4.14.13-0-iitb-amd64_4.14.13-iitb-amd64
#+END_SRC
Then, just run the [[file:update_preseed.sh][update_preseed.sh]] from the parent directory of ~cd~ with
the ~example-preseed.txt~ file present there and it will automatically
update the initrd for preseeding. But to actually ensure that the
preseeding happens, choose the "Install" option in the installer. NOT
"graphical install". Note that this preseed file currently installs
the updated kernel you built above.
* Create a bootable image
This command is adapted from the debian-live project's approach. Run
it from the parent directory of ~cd~

#+BEGIN_SRC shell :exports code
xorriso -outdev test.iso -padding 0 -map cd / -chmod 0755 / -- -boot_image isolinux dir=/isolinux -boot_image isolinux system_area=/usr/lib/ISOLINUX/isohdpfx.bin -boot_image any next -boot_image any efi_path=boot/grub/efi.img -boot_image isolinux partition_entry=gpt_basdat
#+END_SRC

Then use a tool like [[https://etcher.io/][Etcher]] to create a bootable USB drive and test it
out. Ensure that you choose the "Install" option and NOT the graphical
install option to automate the installation.
