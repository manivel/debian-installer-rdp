# Sound confguration
tar -v -z -x -C /target/root -f /cdrom/RDP/sound-config.tar.gz
chroot /target bash /root/hardware/sound/install.sh

# Bluetooth confguration
tar -v -z -x -C /target/root -f /cdrom/RDP/bluetooth-config.tar.gz
chroot /target bash /root/hardware/bluetooth/install.sh

# Miscellaneous
chroot /target adduser rdp sudo

echo deb http://deb.debian.org/debian/ stretch main non-free contrib > /target/etc/apt/sources.list
echo deb http://deb.debian.org/debian/ stretch-backports main non-free contrib >> /target/etc/apt/sources.list
echo deb http://security.debian.org/ stretch/updates main contrib non-free >> /target/etc/apt/sources.list
